# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: BORUAK SERGEY

**E-MAIL**: boosoov@gmail.com

# HARDWARE

**CPU**: Intel Core i3

**RAM**: 2 GB

**ROM**: 1 GB

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN 

```bash
java -jar ./task-manager.jar
```
# LINKS TO SKREENSHOTS
    
JSE-03: https://drive.google.com/drive/folders/1ACPkYtPvJCvT54CS6y00CJc2hnP4QTBp?usp=sharing